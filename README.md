# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

-This project is a web application for stock management. 
-This application allows clients to order articles, view the orders, and manager them (add, modify, delete).
-It also helps the providers to add articles to the stock in order to make it possible to buy them.
-Every article has a categorie. 
-To be able to use the application, you should have a user account.



### Technologies and Environment ###

-Java (jdk 1.8)
-Spring MVC 3.1
-Spring IoC 3.1
-Spring Security 3.2
-JPA/Hibernate 2.0
-Maven 4.0.0
-Bootstrap
-JUnit4
-Mockito

-Xampp
-STS (Spring Tool Suite 3.9.0)
-Tomcat 9
-phpMyAdmin
