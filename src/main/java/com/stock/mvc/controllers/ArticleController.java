package com.stock.mvc.controllers;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;

import com.stock.mvc.entities.Article;
import com.stock.mvc.services.IArticleService;
import com.stock.mvc.services.IFlickrService;
import com.stock.mvc.services.impl.FlickrServiceImpl;

@Controller
@RequestMapping(value = "/article")
public class ArticleController {
	
	@Autowired
	private IArticleService articleService;
	
	@Autowired
	private IFlickrService flickrService;
	
	@RequestMapping (value= "/")
	public String article(Model model ) {
		
		List<Article> articles = articleService.selectAll();
		if (articles == null) {
			articles = new ArrayList<Article>();
		}
		model.addAttribute("articles", articles);
		return "article/article";
	}
	
	

	@RequestMapping (value= "/nouveau", method = RequestMethod.GET)
	public String ajouterArticle(Model model ) {
		Article article = new Article();
		model.addAttribute("article", article);
		return "article/ajouterArticle";
	}
	

	@RequestMapping (value= "/enregister")
	public String enregistrerArticle(Model model, Article article, MultipartFile file) {
		String photoUrl = null;
		if (article !=null) {
			if (file != null && !file.isEmpty()) {
				InputStream stream = null;
				try {
					stream =  file.getInputStream();
				    photoUrl = flickrService.savePhoto(stream, article.getDesignation());
				    article.setPhoto(photoUrl);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} finally {
						try {
							stream.close();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				
				} 

			if (article.getIdArticle() != null) {
				articleService.update(article);
			} else {
			articleService.save(article);
			}
		}
		
		return "redirect:/article/";
	}
	
	
	
	
	
	@RequestMapping (value= "/modifier/{idArticle}")
	public String modifierArticle(Model model, @PathVariable Long idArticle) {
		if(idArticle != null ) {
			Article article = articleService.getById(idArticle);
			if (article != null) {
				model.addAttribute("article", article);
			}
		}
		return "article/ajouterArticle";
	}
	
	@RequestMapping (value= "/supprimer/{idArticle}")
	public String supprimerArticle(Model model, @PathVariable Long idArticle) {
		if(idArticle != null ) {
			Article article = articleService.getById(idArticle);
			if (article != null) {
				// TODO vérification de dépendances avant supression 
				articleService.remove(idArticle);
			}
		}
		return "redirect:/article/";
	}
	
	
	
	
}
