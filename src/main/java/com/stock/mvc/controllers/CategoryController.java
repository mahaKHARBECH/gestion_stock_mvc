package com.stock.mvc.controllers;


import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.stock.mvc.entities.Categorie;
import com.stock.mvc.services.ICategorieService;


@Controller
@RequestMapping(value = "/category")
public class CategoryController {
	
	@Autowired
	private ICategorieService categorieService;
	
	@RequestMapping (value= "/")
	public String categorie(Model model ) {
		
		List<Categorie> categories = categorieService.selectAll();
		if (categories == null) {
			categories = new ArrayList<Categorie>();
		}
		model.addAttribute("categories", categories);
		return "category/category";
	}
	
	

	@RequestMapping (value= "/nouveau", method = RequestMethod.GET)
	public String ajouterCategory(Model model ) {
		Categorie category = new Categorie();
		model.addAttribute("category", category);
		return "category/ajouterCategory";
	}
	

	@RequestMapping (value= "/enregister")
	public String enregistrerCategory(Model model, Categorie categorie) {
		if (categorie !=null) {
			if (categorie.getIdCategorie() != null) {
				categorieService.update(categorie);
			} else {
			categorieService.save(categorie);
			}
		}
		
		return "redirect:/category/";
	}
	
	
	
	
	
	@RequestMapping (value= "/modifier/{idCategorie}")
	public String modifierCategorye(Model model, @PathVariable Long idCategorie) {
		if(idCategorie != null ) {
			Categorie category = categorieService.getById(idCategorie);
			if (category != null) {
				model.addAttribute("category", category);
			}
		}
		return "category/ajouterCategory";
	}
	
	@RequestMapping (value= "/supprimer/{idCategorie}")
	public String supprimerCategory(Model model, @PathVariable Long idCategorie) {
		if(idCategorie != null ) {
			Categorie categorie = categorieService.getById(idCategorie);
			if (categorie != null) {
				// TODO vérification de dépendances avant supression 
				categorieService.remove(idCategorie);
			}
		}
		return "redirect:/category/";
	}
	
	
	
	
}
