package com.stock.mvc.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.stock.mvc.entities.Client;
import com.stock.mvc.entities.CommandeClient;
import com.stock.mvc.entities.LigneCommandeClient;
import com.stock.mvc.services.IArticleService;
import com.stock.mvc.services.IClientService;
import com.stock.mvc.services.ICommandeClientService;
import com.stock.mvc.services.ILigneCommandeClientService;

@Controller
@RequestMapping(value = "/commandeclient")
public class CommandesClientController {
	
	
	@Autowired
	private IClientService clientService;
	
	@Autowired
	private ICommandeClientService commandeClientService;
	
	@Autowired
	private ILigneCommandeClientService ligneCommandeClientService;
	
	@Autowired
	private IArticleService articleService;
	
	
	@RequestMapping(value= "/")
	public String commandeclient(Model model)
	{
		List<CommandeClient> commandeClients = commandeClientService.selectAll();
		if(commandeClients.isEmpty() ) {
			commandeClients = new ArrayList<CommandeClient>();		
			System.out.println("in commandeClients");
		} else {
			for (CommandeClient commandeClient : commandeClients) {
				System.out.println("commandeClients : "+commandeClient.getCode());
				List<LigneCommandeClient> ligneCommandeClients = 
						ligneCommandeClientService.getByIdCommande(commandeClient.getIdCommandeClient());
				
				commandeClient.setLigneCommandeClients(ligneCommandeClients);
			}
		}
		model.addAttribute("commandeClients", commandeClients);
		return "commandeclient/commandeclient";
	}
	
	
	
	@RequestMapping(value= "/nouveau")
	public String ajouterCommande(Model model)
	{
	/*	List<CommandeClient> commandeClients = commandeClientService.selectAll();
		if(commandeClients.isEmpty() ) {
			commandeClients = new ArrayList<CommandeClient>();		
			System.out.println("in commandeClients");
		} else {
			for (CommandeClient commandeClient : commandeClients) {
				System.out.println("commandeClients : "+commandeClient.getCode());
				List<LigneCommandeClient> ligneCommandeClients = 
						ligneCommandeClientService.getByIdCommande(commandeClient.getIdCommandeClient());
				
				commandeClient.setLigneCommandeClients(ligneCommandeClients);
			}
		}
		model.addAttribute("commandeClients", commandeClients);*/
		return "commandeclient/ajoutercommande";
	}
	
	/*@RequestMapping (value= "/nouveau", method = RequestMethod.GET)
	public String ajouterClient(Model model ) {
		Client client = new Client();
		model.addAttribute("client", client);
		System.out.println("in nouveau : "+ client.getNom());
		return "client/ajouterClient";
	}*/
	

}
