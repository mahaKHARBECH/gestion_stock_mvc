package com.stock.mvc.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/commandefournisseur")
public class CommandesFournisseurController {
	
	
	@RequestMapping(value= "/")
	public String commandefournisseur(Model model)
	{
		return "commandefournisseur/commandefournisseur";
	}
	

}
