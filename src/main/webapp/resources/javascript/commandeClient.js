function updateDetailsCommande(idCommande) {
	var json = $.parseJSON($("#json" + idCommande).text());
	
	var detailHTML= "";
	if (json) { 
		for (var index = 0; index < json.length; index++) {
			detailHTML +=
				"<tr>"+
				"<td>"+json[index].article.codeArticle  +"</td>"+
				"<td>"+json[index].quantite  +"</td>"+
				"<td>"+json[index].article.prixUnitaireTTC  +"</td>"+
				"<td>0</td>"+
				"<td></td>"+
			"</tr>";
		}
		
		$("#detailCommande").html(detailHTML);
	
	}
}