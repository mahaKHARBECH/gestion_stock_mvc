package gestion_stock_mvc;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import com.stock.mvc.entities.CommandeClient;
import com.stock.mvc.entities.LigneCommandeClient;

public class CommandeClientTest {
	
	LigneCommandeClient l1,l2;
	List<LigneCommandeClient> lignes;
	CommandeClient commandeClient;

	
	 @Before
		public void initCommandeClientTest(){
		 
		 
		 	l1 = new LigneCommandeClient();// Mockito.mock(LigneCommandeClient.class);
			l2 = new LigneCommandeClient();
			lignes = new ArrayList<LigneCommandeClient>();
			
			BigDecimal bd1 = new BigDecimal (1000);
			BigDecimal bd2 = new BigDecimal (2);
			BigDecimal bd3 = new BigDecimal (25);
			BigDecimal bd4 = new BigDecimal (4);
			
			l1.setPrixUnitaire(bd1);
			l1.setQuantite(bd2);
			
			l2.setPrixUnitaire(bd3);
			l2.setQuantite(bd4);
			
			lignes.add(l1);
		
			commandeClient =new CommandeClient();
			commandeClient.setLigneCommandeClients(lignes);		
	
	}
	
	@Test
	public void testTotalCommande(){	

		assertEquals(new BigDecimal(2000), commandeClient.getTotalCommande());
	}
	
	
	@Test
	public void testTotalCommandePour2Lignes(){	
		
		lignes.add(l2);
		commandeClient.setLigneCommandeClients(lignes);		

		assertEquals(new BigDecimal(2100), commandeClient.getTotalCommande());
	}

}
